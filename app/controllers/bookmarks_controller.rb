class BookmarksController < ApplicationController
  before_action :set_bookmark, only: [:show, :edit, :update, :destroy]

  # GET /bookmarks
  # GET /bookmarks.json
  def index
    @bookmarks = current_user.bookmarks.page(params[:page]).per_page(6)

    if params[:search]
      @bookmarks = current_user.bookmarks.search(params[:search]).page(params[:page]).per_page(6)
    else
      @bookmarks = current_user.bookmarks.page(params[:page]).per_page(6)
    end
  end

  # GET /bookmarks/1
  # GET /bookmarks/1.json
  def show
    @link = LinkThumbnailer.generate(@bookmark.url)
  end

  # GET /bookmarks/new
  def new
    @bookmark = Bookmark.new
  end

  # GET /bookmarks/1/edit
  def edit
  end

  # POST /bookmarks
  # POST /bookmarks.json
  def create
    @bookmark = current_user.bookmarks.create(bookmark_params)

    respond_to do |format|
      if @bookmark.save
        format.html { redirect_to bookmarks_url, notice: 'Bookmark was successfully created.' }
        format.json { render :show, status: :created, location: bookmarks_url }
      else
        format.html { render :new }
        format.json { render json: @bookmark.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookmarks/1
  # PATCH/PUT /bookmarks/1.json
  def update
    respond_to do |format|
      if @bookmark.update(bookmark_params)
        format.html { redirect_to bookmarks_url, notice: 'Bookmark was successfully updated.' }
        format.json { render :show, status: :ok, location: bookmarks_url }
      else
        format.html { render :edit }
        format.json { render json: @bookmark.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookmarks/1
  # DELETE /bookmarks/1.json
  def destroy
    @bookmark.destroy
    respond_to do |format|
      format.html { redirect_to bookmarks_url, notice: 'Bookmark was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_bookmark
      @bookmark = current_user.bookmarks.find(params[:id])
    end

    def bookmark_params
      params.require(:bookmark).permit(:url, :id_user, :screenshot_url)
    end
end
