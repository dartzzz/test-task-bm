class Bookmark < ApplicationRecord
  after_create :screenshot
  validates :url, format: URI::regexp(%w(http https)), presence: true

  belongs_to :user

  def self.search(query)
    where('url LIKE ?', "%#{query}%")
  end

  private

  def url_title
      URI.parse(url).host.sub(/\Awww\./, '')
  end

  def screenshot
    ws = Webshot::Screenshot.instance
    screenshot = ws.capture url, "#{url_title}.png", width: 300, height: 300, quality: 85
    uploader = Cloudinary::Uploader.upload(screenshot.path)
    update(screenshot_url: uploader['url'])
  end
end
