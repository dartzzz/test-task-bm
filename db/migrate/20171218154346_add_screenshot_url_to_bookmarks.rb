class AddScreenshotUrlToBookmarks < ActiveRecord::Migration[5.0]
  def change
    add_column :bookmarks, :screenshot_url, :string
  end
end
