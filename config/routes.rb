Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: "users/omniauth_callbacks" }
  resources :bookmarks
  resource :welcome, only: [:show]

  authenticated :user do
    root 'bookmarks#index', as: :authenticated_root
  end

  root 'welcome#show'
end
